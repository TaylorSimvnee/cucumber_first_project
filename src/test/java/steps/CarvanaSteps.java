package steps;

import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pages.*;
import utils.ActionsHandler;
import utils.Driver;
import utils.Waiter;

public class CarvanaSteps {

    WebDriver driver;
    CarvanaHomePage carvanaHomePage;
    HelpMeSearchPage helpMeSearchPage;
    HelpMeSearchQaPage helpMeSearchQaPage;
    SellMyCarPage sellMyCarPage;
    AutoLoanCalculatorPage autoLoanCalculatorPage;


    @Before
    public void setup(){
        driver = Driver.getDriver();
        carvanaHomePage = new CarvanaHomePage();
        helpMeSearchPage = new HelpMeSearchPage();
        helpMeSearchQaPage = new HelpMeSearchQaPage();
        sellMyCarPage = new SellMyCarPage();
        autoLoanCalculatorPage = new AutoLoanCalculatorPage();
    }

    @Given("user is on {string}")
    public void userNavigatesTo(String url) {
        driver.get(url);
        Waiter.pause(3);
    }

    @When("user clicks on {string} menu item")
    public void userClicksOnMenuItem(String menuItem) {
        switch (menuItem) {
            case "CAR FINDER":
                Waiter.waitForVisibilityOfElement(driver, carvanaHomePage.carFinderItem, 10);
                carvanaHomePage.carFinderItem.click();
                break;
            case "SELL/TRADE":
                carvanaHomePage.sellTradeLink.click();
                break;
            case "AUTO LOAN CALCULATOR":
                carvanaHomePage.autoLoanCalLink.click();
                break;
            default:
                throw new NotFoundException("The text is not defined properly in the feature file!!!");
        }
    }

    @Then("user should be navigated to {string}")
    public void userShouldBeNavigatedTo(String url) {
        Waiter.pause(2);
        Assert.assertEquals(url, driver.getCurrentUrl());
    }

    @And("user should see {string} text")
    public void userShouldSeeText(String text) {
        switch (text){
            case "WHAT CAR SHOULD I GET?":
                Assert.assertTrue(helpMeSearchPage.h1.isDisplayed());
                Assert.assertEquals(text, helpMeSearchPage.h1.getText());
                break;
            case "Car Finder can help! Answer a few quick questions to find the right car for you.":
                Assert.assertTrue(helpMeSearchPage.h3.isDisplayed());
                Assert.assertEquals(text,helpMeSearchPage.h3.getText());
                break;
            case "WHAT IS MOST IMPORTANT TO YOU IN YOUR NEXT CAR?":
                Assert.assertTrue(helpMeSearchQaPage.attributesHeadline.isDisplayed());
                Assert.assertEquals(text,helpMeSearchQaPage.attributesHeadline.getText());
                break;
            case "Select up to 4 in order of importance":
                Assert.assertTrue(helpMeSearchQaPage.attributesSubHeading.isDisplayed());
                Assert.assertEquals(text,helpMeSearchQaPage.attributesSubHeading.getText());
                break;
            case "GET A REAL OFFER IN 2 MINUTES":
                Assert.assertTrue(sellMyCarPage.offerHeading.isDisplayed());
                Assert.assertEquals(text,sellMyCarPage.offerHeading.getText());
                break;
            case "We pick up your car. You get paid on the spot.":
                Assert.assertTrue(sellMyCarPage.paidSubHeading.isDisplayed());
                Assert.assertEquals(text,sellMyCarPage.paidSubHeading.getText());
                break;
            case "We couldn’t find that VIN. Please check your entry and try again.":
                Assert.assertTrue(sellMyCarPage.errorMessage.isDisplayed());
                Assert.assertEquals(text,sellMyCarPage.errorMessage.getText());
                break;
            default:
                throw new NotFoundException("The text is not defined properly in the feature file!!!");
        }
    }

    @And("user should see {string} link")
    public void userShouldSeeLink(String linkText) {
        Assert.assertTrue(helpMeSearchPage.carFinderLink.isDisplayed());
        Assert.assertEquals(linkText, helpMeSearchPage.carFinderLink.getText());
    }

    @When("user clicks on {string} link")
    public void userClicksOnLink(String linkText) {
        helpMeSearchPage.carFinderLink.click();
    }


    @When("user clicks on {string} button")
    public void userClicksOnButton(String buttonText) {
        switch (buttonText) {
            case "GET MY OFFER":
                sellMyCarPage.getOfferButton.click();
            break;
            case "VIN":
                sellMyCarPage.vinButton.click();
                break;
            default:
                throw new NotFoundException("The text is not defined properly in the feature file!!!");
        }
    }

    @And("user enters vin number as {string}")
    public void userEntersVinNumberAs(String vinNumber) {
        sellMyCarPage.vinInputBox.sendKeys(vinNumber);
    }

    @When("user hovers over on {string} menu item")
    public void userHoversOverOnMenuItem(String menuItemText) {
        ActionsHandler.moveToElement(carvanaHomePage.financingDropDown);

    }

    @When("user enters {string} as {string}")
    public void userEntersAs(String formItem, String input) {
        if(formItem.equals("Cost of Car I want")){
            autoLoanCalculatorPage.costInputBox.sendKeys(input);
        }
        else if(formItem.equals("What is Your Down Payment?")){
            autoLoanCalculatorPage.downPaymentInputBox.sendKeys(input);
        }
    }

    @And("user selects {string} as {string}")
    public void userSelectsAs(String formItem, String input) {
         if(formItem.equals("What’s Your credit Score?")){
             for (WebElement e : autoLoanCalculatorPage.creditDropDown) {
                 if (e.getText().equals(input)) e.click();
             }
        }
        else if(formItem.equals("Choose Your Loan Terms")){
             for (WebElement e : autoLoanCalculatorPage.loanTermsDropDown) {
                 if (e.getText().equals(input)) e.click();
             }
        }
    }

    @Then("user should see the monthly payment as {string}")
    public void userShouldSeeTheMonthlyPaymentAs(String text) {
        Assert.assertTrue(autoLoanCalculatorPage.monthlyEstimateText.isDisplayed());
        Assert.assertEquals(text ,autoLoanCalculatorPage.monthlyEstimateText.getText());

    }
}
