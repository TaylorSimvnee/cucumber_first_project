package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.Driver;

import java.util.List;

public class CarvanaHomePage {

    public CarvanaHomePage(){
        PageFactory.initElements(Driver.getDriver(),this);
    }


    @FindBy(css = "a[data-cv-test='headerCarFinderLink']")
    public WebElement carFinderItem;

    @FindBy(css = "a[data-cv-test='headerTradesLink']")
    public WebElement sellTradeLink;

    @FindBy(css = "a[data-cv-test='headerFinanceLoanCalc']")
    public WebElement autoLoanCalLink;


    @FindBy(css = "div[data-qa='header-items']>a")
    public List<WebElement> menuNavigationLinks;

    @FindBy(css = "div[data-cv-test='headerFinanceDropdown']")
    public WebElement financingDropDown;

    @FindBy(css = "a[data-cv-test^='headerFinance']")
    public List<WebElement> financingDropDownItems;





}
