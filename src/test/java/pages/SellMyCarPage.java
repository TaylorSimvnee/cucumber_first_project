package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.Driver;

import java.util.List;

public class SellMyCarPage {
    public SellMyCarPage(){
        PageFactory.initElements(Driver.getDriver(),this);
    }

    @FindBy(css = ".sc-cc10da00-0")
    public WebElement offerHeading;

    @FindBy(css = ".sc-75968884-0")
    public WebElement paidSubHeading;

    @FindBy(css = "button[data-cv-test='VINToggle']")
    public WebElement vinButton;

    @FindBy(css = "button[class^='ToggleOption']")
    public List<WebElement> plateVinButtons;

    @FindBy(css = "div[data-cv-test='heroVinEntryInput']>input")
    public WebElement vinInputBox;

    @FindBy(css = "button[data-cv-test='heroGetMyOfferButton']")
    public WebElement getOfferButton;

    @FindBy(css = ".sc-fTFLOO")
    public WebElement errorMessage;

}
