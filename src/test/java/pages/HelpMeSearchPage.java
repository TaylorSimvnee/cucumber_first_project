package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.Driver;

public class HelpMeSearchPage {
    public HelpMeSearchPage(){
        PageFactory.initElements(Driver.getDriver(),this);
    }


    @FindBy(css = "div>h1")
    public WebElement h1;

    @FindBy(css = "div>h3")
    public WebElement h3;

    @FindBy(css = "a[data-qa='router-link']")
    public WebElement carFinderLink;

}
