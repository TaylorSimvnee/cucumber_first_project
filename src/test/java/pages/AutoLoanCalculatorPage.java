package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.Driver;

import java.util.List;

public class AutoLoanCalculatorPage {
    public AutoLoanCalculatorPage(){
            PageFactory.initElements(Driver.getDriver(),this);
        }


    @FindBy(css = "input[name='vehiclePrice']")
    public WebElement costInputBox;

    @FindBy(css = "#creditBlock>option")
    public List<WebElement> creditDropDown;

    @FindBy(css = "select[name='loanTerm']>option")
    public List<WebElement> loanTermsDropDown;

    @FindBy(css = "input[name='downPayment']")
    public WebElement downPaymentInputBox;

    @FindBy(css = ".loan-calculator-display-value")
    public WebElement monthlyEstimateText;
    }
